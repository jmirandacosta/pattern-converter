/**
 * 
 */
package patterns.converter;

/**
 * @author Jorge
 *
 */
public class InvoiceConverter extends AbstractConverter<Invoice, InvoiceDTO>{

	private final UserConverter userConverter = new UserConverter();
	
	@Override
	public Invoice fromDto(InvoiceDTO dto) {
		// TODO Auto-generated method stub
		Invoice invoice = new Invoice();
		invoice.setUser(userConverter.fromDto(dto.getUser()));
		// ... other setters
		return invoice;
	}

	@Override
	public InvoiceDTO fromEntity(Invoice entity) {
		// TODO Auto-generated method stub
		InvoiceDTO invoice = new InvoiceDTO();
		invoice.setUser(userConverter.fromEntity(entity.getUser()));
		// ... other setters
		return invoice;
	}

}
