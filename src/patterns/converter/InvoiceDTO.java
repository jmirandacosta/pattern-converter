/**
 * 
 */
package patterns.converter;

import java.util.List;

/**
 * @author Jorge
 *
 */
public class InvoiceDTO {
	
	private Long id;
	private UserDTO user;
	private List<String> lines;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public List<String> getLines() {
		return lines;
	}
	public void setLines(List<String> lines) {
		this.lines = lines;
	}

}
