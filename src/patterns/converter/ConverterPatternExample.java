/**
 * 
 */
package patterns.converter;

import java.util.List;

/**
 * @author Jorge
 *
 */
public class ConverterPatternExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new ConverterPatternExample();
		
	}
		
	public ConverterPatternExample() {
		super();
		// TODO Auto-generated constructor stub
		UserDTO userDto = findUserByUsername("dadas");
		System.out.println(userDto.toString());
		save(userDto);
		List<UserDTO> usersDto = findAllUsers();
		System.out.println(usersDto.toString());
	}

	public UserDTO findUserByUsername(String username) {
		UserDao userDao = new UserDaoImpl();
		User user = userDao.findByUsername(username);
		UserConverter converter = new UserConverter();
		return converter.fromEntity(user);
	}
	
	public void save(UserDTO userDto) {
		UserConverter converter = new UserConverter();
		User userEntity = converter.fromDto(userDto);
		UserDao userDao = new UserDaoImpl();
		userDao.save(userEntity);
	}
	
	public List<UserDTO> findAllUsers() {
		UserConverter converter = new UserConverter();
		UserDao userDao = new UserDaoImpl();
		List<User> users = userDao.findAllUsers();	
		return converter.fromEntity(users);
	}

}
