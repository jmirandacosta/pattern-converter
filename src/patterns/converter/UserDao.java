/**
 * 
 */
package patterns.converter;

import java.util.List;

/**
 * @author Jorge
 *
 */
public interface UserDao {
	
	public User findByUsername(String username);
	
	public void save(User user);
	
	public List<User> findAllUsers();

}
