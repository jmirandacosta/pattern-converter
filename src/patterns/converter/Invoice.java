/**
 * 
 */
package patterns.converter;

import java.util.List;

/**
 * @author Jorge
 *
 */
public class Invoice {
	
	private Long id;
	private User user;
	private List<Lines> lines;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Lines> getLines() {
		return lines;
	}
	public void setLines(List<Lines> lines) {
		this.lines = lines;
	}

}
