/**
 * 
 */
package patterns.converter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jorge
 *
 */
public class UserDaoImpl implements UserDao {

	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		List<Role> roles = new ArrayList<Role>();
		roles.add(Role.PLATINUM);
		roles.add(Role.GOLDEN);
		roles.add(Role.STANDAR);
		User user = new User(Long.valueOf(3425), "username", "123456", roles);
		return user;
	}

	@Override
	public void save(User user) {
		// TODO Auto-generated method stub
		System.out.println("Saved: "+ user.toString());
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		List<Role> roles = new ArrayList<Role>();
		roles.add(Role.PLATINUM);
		roles.add(Role.GOLDEN);
		roles.add(Role.STANDAR);
		
		User user1 = new User(Long.valueOf(3425), "username", "123456", roles);
		User user2 = new User(Long.valueOf(5234), "username", "654321", roles);
		
		List<User> users = new ArrayList<User>(); 
		users.add(user1);
		users.add(user2);
		return users;
	}
	
	

}
